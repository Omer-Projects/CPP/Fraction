// Entry point for testing

#include <cstdlib>
#include <iostream>
#include <string.h>
#include <sstream>

#include "doctest.h"

#include "sources/Fraction.hpp"

using namespace doctest;
using namespace ariel;

/**
 * @brief Check Fraction Print
 * 
 * @param f the Fraction
 * @param m expected m
 * @param n expected n
 * 
 * @note it passed if f == m.n
 */
void checkFractionPrint(Fraction f, const char* expected)
{
    std::ostringstream oss;
    std::streambuf* coutStreambuf = std::cout.rdbuf();
    std::cout.rdbuf(oss.rdbuf());

    std::cout << f;

    std::cout.rdbuf(coutStreambuf); // restore

    CHECK(oss.str() == std::string(expected));
    std::cout << oss.str();
}

TEST_CASE("build")
{
    Fraction f1;
    Fraction f2(1.5);
    Fraction f3(1.999), f4(1.9999), f5(-1.1245);
    Fraction f6(1, 5), f7(-1, 45), f8(-10, 10001);
    Fraction f9(f2);
    Fraction f10 = f2;

    checkFractionPrint(f1, "0");
    std::cout << " ";
    checkFractionPrint(f2, "3/2");
    std::cout << " ";
    checkFractionPrint(f3, "1999/1000");
    std::cout << " ";
    checkFractionPrint(f4, "1999/1000");
    std::cout << " ";
    checkFractionPrint(f5, "-281/250");
    std::cout << " ";
    checkFractionPrint(f6, "1/5");
    std::cout << " ";
    checkFractionPrint(f7, "-1/45");
    std::cout << " ";
    checkFractionPrint(f8, "-10/10001");
    std::cout << std::endl;

    checkFractionPrint(f9, "3/2");
    std::cout << " ";
    checkFractionPrint(f10, "3/2");
    std::cout << std::endl;

    try
    {
        Fraction f(1, 0);
        CHECK(false);
    }
    catch(const std::exception& e) { }
    
}

TEST_CASE("plus operation")
{
    Fraction f1, f2(3.5), f3(-0.5), f4(0.001);

    checkFractionPrint(f1 + f1, "0");
    std::cout << " ";
    checkFractionPrint(f2 + f3, "3/1");
    std::cout << " ";
    checkFractionPrint(f2 + -0.5, "3/1");
    std::cout << " ";
    checkFractionPrint(3.5 + f3, "3/1");
    std::cout << " ";
    checkFractionPrint(f2 + f3 + f4, "3001/1000");
    std::cout << " ";
    checkFractionPrint(f2 + -0.5 + f4, "3001/1000");
    std::cout << std::endl;

    checkFractionPrint(f1 + Fraction(1.5), "3/2");
    std::cout << " ";
    checkFractionPrint(f1 + 1.5, "3/2");
    std::cout << " ";
    checkFractionPrint(Fraction(3.5) + f3, "3/1");
    std::cout << " ";
    checkFractionPrint(3.5 + f3, "3/1");
    std::cout << " ";
    checkFractionPrint(f2 + Fraction(-0.5) + f4, "3001/1000");
    std::cout << " ";
    checkFractionPrint(f2 + -0.5 + f4, "3001/1000");
    std::cout << " ";
    checkFractionPrint(Fraction(4.5) + Fraction(1.5), "6/1");
    std::cout << " ";
    checkFractionPrint(4.5 + Fraction(1.5), "6/1");
    std::cout << " ";
    checkFractionPrint(Fraction(4.5) + 1.5, "6/1");
    std::cout << std::endl;

    checkFractionPrint(++f3, "1/2");
    std::cout << " ";
    checkFractionPrint(++(++f3), "5/2");
    std::cout << " ";
    checkFractionPrint(f3++, "5/2");
    std::cout << std::endl;

    f1 = Fraction();
    f2 = Fraction(5, 2);
    checkFractionPrint(f1, "0");
    std::cout << " ";
    f1 += Fraction(1.0);
    checkFractionPrint(f1, "1/1");
    std::cout << " ";
    f1 += f2;
    checkFractionPrint(f1, "7/2");
    std::cout << " ";
    f1 += f1 + Fraction(1, 4);
    checkFractionPrint(f1, "29/4");
    std::cout << " ";
    f1 += f1 += f1;
    checkFractionPrint(f1, "29/1");
    std::cout << " ";
    checkFractionPrint(f1 += Fraction(13, 4), "129/4");
    std::cout << std::endl;

    f1 = Fraction();
    f2 = Fraction(5, 2);
    checkFractionPrint(f1, "0");
    std::cout << " ";
    f1 +=1;
    checkFractionPrint(f1, "1/1");
    std::cout << " ";
    f1 += f2;
    checkFractionPrint(f1, "7/2");
    std::cout << " ";
    f1 += f1 +0.25;
    checkFractionPrint(f1, "29/4");
    std::cout << " ";
    f1 += f1 += f1;
    checkFractionPrint(f1, "29/1");
    std::cout << " ";
    checkFractionPrint(f1 += 3.25, "129/4");
    std::cout << std::endl;
}

TEST_CASE("minus operation")
{
    Fraction f1, f2(3.5), f3(-0.5), f4(0.001);

    checkFractionPrint(f1 - f1, "0");
    std::cout << " ";
    checkFractionPrint(f2 - f3, "4/1");
    std::cout << " ";
    checkFractionPrint(f2 - f3 - f4, "3999/1000");
    std::cout << std::endl;

    checkFractionPrint(f1 - Fraction(1.5), "-3/2");
    std::cout << " ";
    checkFractionPrint(f1 - 1.5, "-3/2");
    std::cout << " ";
    checkFractionPrint(Fraction(3.5) - f3, "4/1");
    std::cout << " ";
    checkFractionPrint(3.5 - f3, "4/1");
    std::cout << " ";
    checkFractionPrint(f2 - Fraction(2,2) - f4, "2499/1000");
    std::cout << " ";
    checkFractionPrint(f2 - 1 - f4, "2499/1000");
    std::cout << " ";
    checkFractionPrint(Fraction(4.5) - Fraction(1.5), "3/1");
    std::cout << std::endl;

    checkFractionPrint(--f3, "-3/2");
    std::cout << " ";
    checkFractionPrint(--(--f3), "-7/2");
    std::cout << " ";
    checkFractionPrint(f3--, "-7/2");
    std::cout << std::endl;

    f1 = Fraction();
    f2 = Fraction(5, 2);
    checkFractionPrint(f1, "0");
    std::cout << " ";
    f1 -= Fraction(1.0);
    checkFractionPrint(f1, "-1/1");
    std::cout << " ";
    f1 -= f2;
    checkFractionPrint(f1, "-7/2");
    std::cout << " ";
    f1 -= f1 - Fraction(1, 4);
    checkFractionPrint(f1, "1/4");
    std::cout << " ";
    f1 -= f1 -= f1;
    checkFractionPrint(f1, "0");
    std::cout << " ";
    checkFractionPrint(f1 -= Fraction(13, 4), "-13/4");
    std::cout << std::endl;

    f1 = Fraction();
    f2 = Fraction(5, 2);
    checkFractionPrint(f1, "0");
    std::cout << " ";
    f1 -= 1;
    checkFractionPrint(f1, "-1/1");
    std::cout << " ";
    f1 -= f2;
    checkFractionPrint(f1, "-7/2");
    std::cout << " ";
    f1 -= f1 - 0.25;
    checkFractionPrint(f1, "1/4");
    std::cout << " ";
    f1 -= f1 -= f1;
    checkFractionPrint(f1, "0");
    std::cout << " ";
    checkFractionPrint(f1 -= 3.25, "-13/4");
    std::cout << std::endl;
}

TEST_CASE("multiply operation")
{
    Fraction f1(1), f2(3.5), f3(-0.5), f4(0.01);

    checkFractionPrint(f1 * f1, "1/1");
    std::cout << " ";
    checkFractionPrint(f2 * f3, "-7/4");
    std::cout << " ";
    checkFractionPrint(f2 * f3 * f4, "-7/400");
    std::cout << std::endl;

    checkFractionPrint(f1 * Fraction(1.5), "3/2");
    std::cout << " ";
    checkFractionPrint(f1 * 1.5, "3/2");
    std::cout << " ";
    checkFractionPrint(Fraction(3.5) * f3, "-7/4");
    std::cout << " ";
    checkFractionPrint(3.5 * f3, "-7/4");
    std::cout << " ";
    checkFractionPrint(f2 * Fraction(-0.5) * f1, "-7/4");
    std::cout << " ";
    checkFractionPrint(f2 * -0.5 * f1, "-7/4");
    std::cout << " ";
    checkFractionPrint(Fraction(4.5) * Fraction(1.5), "27/4");
    std::cout << std::endl;

    f1 = Fraction(2);
    f2 = Fraction(5, 4);
    checkFractionPrint(f1, "2/1");
    std::cout << " ";
    f1 *= Fraction(0.25);
    checkFractionPrint(f1, "1/2");
    std::cout << " ";
    f1 *= f2;
    checkFractionPrint(f1, "5/8");
    std::cout << " ";
    f1 *= f1 * Fraction(8, 1);
    checkFractionPrint(f1, "25/8");
    std::cout << " ";
    f1 = Fraction(1, 2);
    f1 *= f1 *= f1;
    checkFractionPrint(f1, "1/16");
    std::cout << " ";
    checkFractionPrint(f1 *= Fraction(1, 2), "1/32");
    std::cout << std::endl;

    f1 = Fraction(2);
    f2 = Fraction(5, 4);
    checkFractionPrint(f1, "2/1");
    std::cout << " ";
    f1 *= 0.25;
    checkFractionPrint(f1, "1/2");
    std::cout << " ";
    f1 *= f2;
    checkFractionPrint(f1, "5/8");
    std::cout << " ";
    f1 *= f1 * 8;
    checkFractionPrint(f1, "25/8");
    std::cout << " ";
    f1 = Fraction(1, 2);
    f1 *= f1 *= f1;
    checkFractionPrint(f1, "1/16");
    std::cout << " ";
    checkFractionPrint(f1 *= 0.5, "1/32");
    std::cout << std::endl;
}

TEST_CASE("deviation operation")
{
    Fraction f1(1), f2(3.5), f3(-0.5), f4(100);

    checkFractionPrint(f1 / f1, "1/1");
    std::cout << " ";
    checkFractionPrint(f2 / f3, "-7/1");
    std::cout << " ";
    checkFractionPrint(f2 / f3 / f4, "-7/100");
    std::cout << std::endl;

    checkFractionPrint(f1 / Fraction(5,2), "2/5");
    std::cout << " ";
    checkFractionPrint(f1 / 2.5, "2/5");
    std::cout << " ";
    checkFractionPrint(Fraction(3.5) / f4, "7/200");
    std::cout << " ";
    checkFractionPrint(3.5 / f4, "7/200");
    std::cout << " ";
    checkFractionPrint(f2 / Fraction(-0.5) / f4, "-7/100");
    std::cout << " ";
    checkFractionPrint(f2 / -0.5 / f4, "-7/100");
    std::cout << " ";
    checkFractionPrint(Fraction(4.5) / Fraction(1, 4), "18/1");
    std::cout << std::endl;

    f1 = Fraction(2);
    f2 = Fraction(4, 5);
    checkFractionPrint(f1, "2/1");
    f1 /= Fraction(4);
    checkFractionPrint(f1, "1/2");
    std::cout << " ";
    f1 /= f2;
    checkFractionPrint(f1, "5/8");
    std::cout << " ";
    f1 /= f1 / Fraction(1, 8);
    checkFractionPrint(f1, "1/8");
    std::cout << " ";
    f1 = Fraction(2, 1);
    f1 /= f1 /= f1;
    checkFractionPrint(f1, "1/1");
    std::cout << " ";
    checkFractionPrint(f1 /= Fraction(2, 1), "1/2");
    std::cout << std::endl;

    f1 = Fraction(2);
    f2 = Fraction(4, 5);
    checkFractionPrint(f1, "2/1");
    f1 /= 4;
    checkFractionPrint(f1, "1/2");
    std::cout << " ";
    f1 /= f2;
    checkFractionPrint(f1, "5/8");
    std::cout << " ";
    f1 /= f1 / 0.125;
    checkFractionPrint(f1, "1/8");
    std::cout << " ";
    f1 = Fraction(2, 1);
    f1 /= f1 /= f1;
    checkFractionPrint(f1, "1/1");
    std::cout << " ";
    checkFractionPrint(f1 /= 2, "1/2");
    std::cout << std::endl;

    try
    {
        Fraction f(1, 0);
        CHECK(false);
    }
    catch(const std::exception& e) { }

    try
    {
        Fraction f = 1 / Fraction();
        CHECK(false);
    }
    catch(const std::exception& e) { }

    try
    {
        Fraction f = Fraction() / Fraction();
        CHECK(false);
    }
    catch(const std::exception& e) { }

    try
    {
        Fraction f = Fraction(1) / 0;
        CHECK(false);
    }
    catch(const std::exception& e) { }

    try
    {
        Fraction f;
        f /= f;
        CHECK(false);
    }
    catch(const std::exception& e) { }
    try
    {
        Fraction f;
        f /= Fraction();
        CHECK(false);
    }
    catch(const std::exception& e) { }

    try
    {
        Fraction f;
        f /= 0;
        CHECK(false);
    }
    catch(const std::exception& e) { }
}

TEST_CASE("compare operations")
{
    Fraction f1(1, 4), f2(1, 4), f3(1, 5);

    CHECK(f1 == f2);
    CHECK(!(f1 == f3));
    CHECK(f1 == Fraction(1, 4));
    CHECK(f1 == 0.25);
    CHECK(!(f1 == Fraction(1, 5)));
    CHECK(!(f1 == 0.2));
    CHECK(Fraction(1, 4) == f1);
    CHECK(0.25 == f1);
    CHECK(!(Fraction(1, 5) == f1));
    CHECK(!(0.2 == f1));
    CHECK(Fraction(1, 4) == Fraction(1, 4));
    CHECK(0.25 == Fraction(1, 4));
    CHECK(!(Fraction(1, 4) == Fraction(1, 5)));
    CHECK(!(0.25 == Fraction(1, 5)));

    CHECK(!(f1 != f2));
    CHECK(f1 != f3);
    CHECK(!(f1 != Fraction(1, 4)));
    CHECK(!(f1 != 0.25));
    CHECK(f1 != Fraction(1, 5));
    CHECK(f1 != 0.2);
    CHECK(!(Fraction(1, 4) != f1));
    CHECK(!(0.25 != f1));
    CHECK(Fraction(1, 5) != f1);
    CHECK(0.2 != f1);
    CHECK(!(Fraction(1, 4) != Fraction(1, 4)));
    CHECK(!(0.25 != Fraction(1, 4)));
    CHECK(Fraction(1, 4) != Fraction(1, 5));
    CHECK(0.25 != Fraction(1, 5));

    CHECK(f1 <= f2);
    CHECK(f2 >= f1);
    CHECK(f2 <= f1);
    CHECK(f1 >= f2);
    CHECK(f3 < f1);
    CHECK(f1 > f3);
    CHECK(!(f1 < f3));
    CHECK(!(f3 > f1));
    CHECK(!(f1 < f2));
    CHECK(!(f2 > f1));
    CHECK(!(f2 < f1));
    CHECK(!(f1 > f2));
}

TEST_CASE("Fraction E2E")
{
    Fraction a(5,3), b(14,21);
    checkFractionPrint(a, "5/3");
    checkFractionPrint(b, "2/3");
    std::cout << "\n";
    std::cout << "a + b = ";
    checkFractionPrint(a+b, "7/3");
    std::cout << "\n";
    std::cout << "a - b = ";
    checkFractionPrint(a-b, "1/1");
    std::cout << "\n";
    std::cout << "a / b = ";
    checkFractionPrint(a/b, "5/2");
    std::cout << "\n";
    std::cout << "a * b = ";
    checkFractionPrint(a*b, "10/9");
    std::cout << "\n";
    std::cout << "2.3 * b = ";
    checkFractionPrint(2.3*b, "23/15");
    std::cout << "\n";
    std::cout << "a + 2.421 = ";
    checkFractionPrint(a+2.421, "12263/3000");
    std::cout << "\n";
    std::cout << "c = a + b - 1 = ";
    Fraction c = a+b-1;
    checkFractionPrint(c, "4/3");
    std::cout << "\n";
    std::cout << "c++ = ";
    checkFractionPrint(c++, "4/3");
    std::cout << "\n";
    std::cout << "--c = ";
    checkFractionPrint(--c, "4/3");
    std::cout << "\n";

    CHECK(c >= b);
    CHECK(a > 1.1);
}