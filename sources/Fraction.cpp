#include "Fraction.hpp" 

#include <limits>
#include <stdexcept>

namespace ariel
{
    long long gcd(long long n, long long m)
    {
        if (n == 0 || m == 0) {
            return 1;
        }

        if (n < 0) {
            n *= -1;
        }
        
        if (m < 0) {
            m *= -1;
        }

        while (!((n >= m) && ((n%m)==0)))
        {
            long long t = m;
            m = n%m;
            n = t;
        }

        return m;
    }

    void valid_overflow(long long n, long long m)
    {
        if (n > std::numeric_limits<int>::max() || m > std::numeric_limits<int>::max()) {
            throw std::overflow_error("");
        }

        if (n < std::numeric_limits<int>::min() || m < std::numeric_limits<int>::min()) {
            throw std::overflow_error("");
        }
    }

    Fraction::Fraction()
        : m_n(0), m_m(1) { }
    
    Fraction::Fraction(int numerator, int denominator)
        : m_n(numerator), m_m(denominator)
    {
        if (denominator == 0) {
            throw std::invalid_argument("Attempted to divide by zero!");
        }

        fix();
    }

    Fraction::Fraction(float value)
        : Fraction((int)(value * 1000), 1000) { }

    Fraction& Fraction::fix()
    {
        if (m_n == 0)
        {
            m_m = 1;
        }
        else
        {
            int g = gcd(m_n, m_m);
            m_n /= g;
            m_m /= g;

            if (m_m < 0)
            {
                m_m *= -1;
                m_n *= -1;
            }
        }

        return *this;
    }

    Fraction operator+(Fraction fracA, Fraction fracB)
    {
        long long n = (long long)fracA.m_n * (long long)fracB.m_m + (long long)fracB.m_n * (long long)fracA.m_m;
        long long m = (long long)fracA.m_m * (long long)fracB.m_m;

        valid_overflow(n, m);

        return Fraction(n, m);
    }

    Fraction& Fraction::operator+=(Fraction other)
    {
        Fraction res = *this + other;
        m_n = res.m_n;
        m_m = res.m_m;
        return *this;
    }

    Fraction operator-(Fraction fracA, Fraction fracB)
    {
        long long n = (long long)fracA.m_n * (long long)fracB.m_m - (long long)fracB.m_n * (long long)fracA.m_m;
        long long m = (long long)fracA.m_m * (long long)fracB.m_m;

        valid_overflow(n, m);

        return Fraction(n, m);
    }

    Fraction& Fraction::operator-=(Fraction other)
    {
        Fraction res = *this - other;
        m_n = res.m_n;
        m_m = res.m_m;
        return *this;
    }

    Fraction operator*(Fraction fracA, Fraction fracB)
    {
        long long n = (long long)fracA.m_n * (long long)fracB.m_n;
        long long m = (long long)fracA.m_m * (long long)fracB.m_m;

        valid_overflow(n, m);

        return Fraction(n, m);
    }

    Fraction& Fraction::operator*=(Fraction other)
    {
        Fraction res = *this * other;
        m_n = res.m_n;
        m_m = res.m_m;
        return *this;
    }

    Fraction operator/(Fraction fracA, Fraction fracB)
    {
        if (fracB.m_n == 0) {
            throw std::runtime_error("Attempted to divide by zero!");
        }

        long long n = (long long)fracA.m_n * (long long)fracB.m_m;
        long long m = (long long)fracA.m_m * (long long)fracB.m_n;

        valid_overflow(n, m);

        return Fraction(n, m);
    }

    Fraction& Fraction::operator/=(Fraction other)
    {
        Fraction res = *this / other;
        m_n = res.m_n;
        m_m = res.m_m;
        return *this;
    }

    Fraction Fraction::operator++(int)
    {
        Fraction old = *this;
        *this += Fraction(1, 1);
        return old;
    }

    Fraction Fraction::operator--(int)
    {
        Fraction old = *this;
        *this -= Fraction(1, 1);
        return old;
    }

    bool operator>(Fraction fracA, Fraction fracB)
    {
        return fracA.m_n * fracB.m_m > fracB.m_n * fracA.m_m;
    }

    bool operator>=(Fraction fracA, Fraction fracB)
    {
        return fracA.m_n * fracB.m_m >= fracB.m_n * fracA.m_m;
    }

    std::istream& operator>>(std::istream& stream, Fraction& frac)
    {
        int n, m;
        
        stream >> n >> m;

        if (stream.fail())
        {
            throw std::runtime_error("Invalid input");
        }
                
        if (m == 0)
        {
            throw std::runtime_error("Attempted to divide by zero!");
        }

        frac = Fraction(n, m);
        return stream;
    }

    std::ostream& operator<<(std::ostream& stream, const Fraction& frac)
    {
        if (frac.m_n == 0) {
            stream << "0";
            return stream;
        }

        stream << frac.m_n << "/" << frac.m_m;
        return stream;
    }
}
