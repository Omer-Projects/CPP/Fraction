// Fraction

#include <iostream>

namespace ariel
{
    class Fraction
    {
    private:
        int m_n; // Numerator
        int m_m; // Denominator

    public:
        /**
         * @brief Construct a new Fraction with zero as value
         * 
         */
        Fraction();

        /**
         * @brief Construct a new Fraction with n/m
         * 
         * @param numerator of the value
         * @param denominator of the value
         */
        Fraction(int numerator, int denominator);

        /**
         * @brief Construct a new Fraction from float
         * 
         * @param value float value
         * 
         * @note Removes below three digits after the zero
         */
        Fraction(float value);

        friend Fraction operator+(Fraction fracA, Fraction fracB);
        friend inline Fraction operator+(Fraction fracA, float fracB) { return fracA + Fraction( fracB); }
        friend inline Fraction operator+( float fracA, Fraction fracB) { return Fraction(fracA) + fracB; }
        
        Fraction& operator+=(Fraction other);
        inline Fraction& operator+=(float other) { *this += Fraction(other); return *this; }

        friend Fraction operator-(Fraction fracA, Fraction fracB);
        friend inline Fraction operator-(Fraction fracA, float fracB) { return fracA - Fraction( fracB); }
        friend inline Fraction operator-( float fracA, Fraction fracB) { return Fraction(fracA) - fracB; }

        Fraction& operator-=(Fraction other);
        inline Fraction& operator-=(float other) { *this -= Fraction(other); return *this; }

        friend Fraction operator*(Fraction fracA, Fraction fracB);
        friend inline Fraction operator*(Fraction fracA, float fracB) { return fracA * Fraction( fracB); }
        friend inline Fraction operator*( float fracA, Fraction fracB) { return Fraction(fracA) * fracB; }
        
        Fraction& operator*=(Fraction other);
        inline Fraction& operator*=(float other) { *this *= Fraction(other); return *this; }

        friend Fraction operator/(Fraction fracA, Fraction fracB);
        friend inline Fraction operator/(Fraction fracA, float fracB) { return fracA / Fraction( fracB); }
        friend inline Fraction operator/( float fracA, Fraction fracB) { return Fraction(fracA) / fracB; }

        Fraction& operator/=(Fraction other);
        inline Fraction& operator/=(float other) { *this /= Fraction(other); return *this; }

        inline Fraction& operator++() { return (*this += Fraction(1, 1)); }
		Fraction operator++(int);

        inline Fraction& operator--() { return (*this -= Fraction(1, 1)); }
		Fraction operator--(int);

        // friend inline bool operator==(Fraction fracA, Fraction fracB) { return fracA.m_n == fracB.m_n && fracA.m_m == fracB.m_m; }
        friend inline bool operator==(Fraction fracA, Fraction fracB)
        {
            const float UNIT = 1000.0;
            int num1 = (int)(((float)fracA.m_n / (float)fracA.m_m) * UNIT);
            int num2 = (int)(((float)fracB.m_n / (float)fracB.m_m) * UNIT);
            return num1 == num2;
        }
        friend inline bool operator==(Fraction fracA, float fracB) { return fracA == Fraction( fracB); }
        friend inline bool operator==( float fracA, Fraction fracB) { return Fraction(fracA) == fracB; }

        friend inline bool operator!=(Fraction fracA, Fraction fracB) { return !(fracA == fracB); }
        friend inline bool operator!=(Fraction fracA, float fracB) { return !(fracA == fracB); }
        friend inline bool operator!=( float fracA, Fraction fracB) { return !(fracA == fracB); }

        friend bool operator>(Fraction fracA, Fraction fracB);
        friend inline bool operator>(Fraction fracA, float fracB) { return fracA > Fraction( fracB); }
        friend inline bool operator>( float fracA, Fraction fracB) { return Fraction(fracA) > fracB; }

        friend bool operator>=(Fraction fracA, Fraction fracB);
        friend inline bool operator>=(Fraction fracA, float fracB) { return fracA >= Fraction( fracB); }
        friend inline bool operator>=( float fracA, Fraction fracB) { return Fraction(fracA) >= fracB; }

        friend inline bool operator<(Fraction fracA, Fraction fracB) { return !(fracA >= fracB); }
        friend inline bool operator<(Fraction fracA, float fracB) { return !(fracA >= fracB); }
        friend inline bool operator<( float fracA, Fraction fracB) { return !(fracA >= fracB); }

        friend inline bool operator<=(Fraction fracA, Fraction fracB) { return !(fracA > fracB); }
        friend inline bool operator<=(Fraction fracA, float fracB) { return !(fracA > fracB); }
        friend inline bool operator<=( float fracA, Fraction fracB) { return !(fracA > fracB); }

        friend std::istream& operator>>(std::istream& stream, Fraction& franc);
        friend std::ostream& operator<<(std::ostream& stream, const Fraction& franc);

        inline int getNumerator() const { return m_n; }
        inline int getDenominator() const { return m_m; }

    private:
        Fraction& fix();
    };
}
